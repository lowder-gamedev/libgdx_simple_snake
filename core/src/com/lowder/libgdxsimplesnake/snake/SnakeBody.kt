package com.lowder.libgdxsimplesnake.snake

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.lowder.libgdxsimplesnake.config.GRID_SIZE
import com.lowder.libgdxsimplesnake.config.SpriteWithShadow
import com.lowder.libgdxsimplesnake.utils.getRotationCust

/**
 * Created by AntonyCS on 07/04/2024
 */
class SnakeBody(
    private var texture: Texture?,
    private var pos: Vector2,
    private var bodyType: Int
) {
    private var prevPos: Vector2 = pos
    private var spriteWithShadow: SpriteWithShadow

    init {
        val textureRectangle = getBodyTextureRegion(bodyType)
        spriteWithShadow = SpriteWithShadow(
            Sprite(texture, textureRectangle.x.toInt(), textureRectangle.y.toInt(), textureRectangle.width.toInt(), textureRectangle.height.toInt()).apply {
                setSize(GRID_SIZE, GRID_SIZE)
                setOriginCenter()
                setPosition(pos.x, pos.y)
            }
        )
    }

    private fun getBodyTextureRegion(bodyType: Int): Rectangle {
        var textureRectangle = Rectangle(23f, 0f, 23f, 23f)
        TEXTURE_REGIONS[bodyType]?.let {
            textureRectangle = it
        }
        return textureRectangle
    }

    fun changeBodyType(bodyType: Int) {
        this.bodyType = bodyType
        val textureRectangle = getBodyTextureRegion(this.bodyType)

        spriteWithShadow.mainSprite.apply {
            setRegion(textureRectangle.x.toInt(), textureRectangle.y.toInt(), textureRectangle.width.toInt(), textureRectangle.height.toInt())
            setSize(GRID_SIZE, GRID_SIZE)
            setOriginCenter()
            setPosition(pos.x, pos.y)
        }
        spriteWithShadow.update()
    }

    fun getPrevPos() = prevPos
    fun getPos() = pos

    fun getBodyType() = bodyType

    fun getRectangle() = spriteWithShadow.getRectangle()

    fun update(move: Int, nextPos: Vector2?) {
        prevPos = Vector2(pos)
        if (bodyType == HEAD_BODY_TYPE) {
            when (move) {
                Snake.MOVE_UP -> {
                    pos.y += GRID_SIZE
                    spriteWithShadow.mainSprite.rotation = 0f
                }
                Snake.MOVE_DOWN -> {
                    pos.y -= GRID_SIZE
                    spriteWithShadow.mainSprite.rotation = 180f
                }
                Snake.MOVE_LEFT -> {
                    pos.x -= GRID_SIZE
                    spriteWithShadow.mainSprite.rotation = 90f
                }
                Snake.MOVE_RIGHT -> {
                    pos.x += GRID_SIZE
                    spriteWithShadow.mainSprite.rotation = 270f
                }
            }
        } else {
            nextPos?.let {
                pos = it
            }
        }

        spriteWithShadow.mainSprite.setPosition(pos.x, pos.y)
        spriteWithShadow.update()
    }

    fun draw(batch: SpriteBatch?, nextPos: Vector2?) {
        spriteWithShadow.drawShadow(batch)

        if (bodyType != HEAD_BODY_TYPE) {
            nextPos?.let {
                spriteWithShadow.mainSprite.rotation = pos.getRotationCust(it)
                spriteWithShadow.update()
            }
        }
        spriteWithShadow.draw(batch)
    }

    fun dispose() {
        texture?.dispose()
    }

    companion object {
        const val HEAD_BODY_TYPE = 0
        const val MIDDLE_BODY_TYPE = 1
        const val TAIL_BODY_TYPE = 2

        val TEXTURE_REGIONS = mapOf(
            HEAD_BODY_TYPE to Rectangle(0f, 0f, 23f, 23f),
            MIDDLE_BODY_TYPE to Rectangle(23f, 0f, 23f, 23f),
            TAIL_BODY_TYPE to Rectangle(46f, 0f, 23f, 23f),
        )
    }
}