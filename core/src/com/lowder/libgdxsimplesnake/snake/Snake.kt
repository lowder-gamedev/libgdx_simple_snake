package com.lowder.libgdxsimplesnake.snake

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.lowder.libgdxsimplesnake.config.GRID_SIZE
import com.lowder.libgdxsimplesnake.food.Food

/**
 * Created by AntonyCS on 31/03/2024
 */
class Snake(
    pos: Vector2
) {
    private var texture: Texture = Texture("snake/snake_1.png")

    private var allowChangeMove = true
    private var currentMove = MOVE_UP
    private var forbiddenMove = MOVE_DOWN
    private var pendingGrowPoint = 0

    private var updateTime: Float = 0f
    private var speedMod: Float = 0f
    private var maxSpeedMod: Float = 800f
    private var snakes: MutableList<SnakeBody> = mutableListOf()

    init {
        snakes.add(SnakeBody(texture, Vector2(pos.x, pos.y), SnakeBody.HEAD_BODY_TYPE))
        snakes.add(SnakeBody(texture, Vector2(pos.x, pos.y - (GRID_SIZE * 1)), SnakeBody.MIDDLE_BODY_TYPE))
        snakes.add(SnakeBody(texture, Vector2(pos.x, pos.y - (GRID_SIZE * 2)), SnakeBody.MIDDLE_BODY_TYPE))
        snakes.add(SnakeBody(texture, Vector2(pos.x, pos.y - (GRID_SIZE * 3)), SnakeBody.TAIL_BODY_TYPE))
    }

    fun getAllBodyPosition() = snakes.map { it.getPos() }

    fun setMove(currentMove: Int, forbiddenMove: Int) {
        if (currentMove != this.forbiddenMove && allowChangeMove) {
            this.currentMove = currentMove
            this.forbiddenMove = forbiddenMove
            this.allowChangeMove = false
        }
    }

    fun update(delta: Float, foods: MutableList<Food>, callbackCollide: () -> Unit) {
        updateTime += delta
        if (updateTime > (1f - (speedMod * 0.001f))) {
            updateTime = 0f
            allowChangeMove = true

            var currentPos: Vector2? = null
            snakes.forEach {
                it.update(currentMove, currentPos)
                currentPos = Vector2(it.getPrevPos())

                if (it.getBodyType() == SnakeBody.HEAD_BODY_TYPE) {
                    foods.forEach { food ->
                        if (it.getRectangle().overlaps(food.getRectangle()) && food.isAlive) {
                            food.isAlive = false
                            pendingGrowPoint += food.growPoint
                            callbackCollide.invoke()
                        }
                    }
                }
            }

            if (pendingGrowPoint > 0) {
                pendingGrowPoint--

                snakes[snakes.lastIndex].changeBodyType(SnakeBody.MIDDLE_BODY_TYPE)
                snakes.add(SnakeBody(texture, Vector2(currentPos), SnakeBody.TAIL_BODY_TYPE))
                speedMod += 5f
                if (speedMod > maxSpeedMod) {
                    speedMod = maxSpeedMod
                }
            }
        }
    }

    fun draw(batch: SpriteBatch?) {
        var nextPos: Vector2? = null
        snakes.forEach {
            it.draw(batch, nextPos)
            nextPos = it.getPos()
        }
    }

    fun dispose() {
        texture.dispose()
        snakes.forEach { it.dispose() }
    }

    companion object {
        const val MOVE_UP = 0
        const val MOVE_DOWN = 1
        const val MOVE_LEFT = 2
        const val MOVE_RIGHT = 3
    }
}