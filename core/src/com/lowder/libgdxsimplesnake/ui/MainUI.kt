package com.lowder.libgdxsimplesnake.ui

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.lowder.libgdxsimplesnake.config.ARENA_WIDTH
import com.lowder.libgdxsimplesnake.config.UI_MAX_HEIGHT

/**
 * Created by AntonyCS on 13/04/2024
 */
class MainUI {

    private var texture: Texture = Texture("badlogic.jpg")

    fun draw(batch: SpriteBatch?) {
        batch?.draw(texture, 0f, 0f, ARENA_WIDTH.toFloat(), UI_MAX_HEIGHT)
    }
}