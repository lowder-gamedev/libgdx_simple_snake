package com.lowder.libgdxsimplesnake.config

/**
 * Created by AntonyCS on 07/04/2024
 */
const val GRID_SIZE = 20f

const val ARENA_WIDTH = 440
const val ARENA_HEIGHT = 782

const val UI_MAX_HEIGHT = 240f

const val CAMERA_OFFSET_Y = -2f

const val GRID_MIN_X = 0
const val GRID_MIN_Y = 12
const val GRID_MAX_X = 21 // Actual MAX GRID_MAX_X = 22
const val GRID_MAX_Y = 38 // Actual Max GRID_MAX_Y = 39

const val SNAKE_SPAWN_X = 200f
const val SNAKE_SPAWN_Y = 380f