package com.lowder.libgdxsimplesnake.config

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle

/**
 * Created by AntonyCS on 07/04/2024
 */
class SpriteWithShadow(
    var mainSprite: Sprite,
    var size: Float = GRID_SIZE
) {

    private lateinit var shadowSprite: Sprite
    private lateinit var rectangle: Rectangle

    init {
        shadowSprite = Sprite(mainSprite.texture, mainSprite.regionX, mainSprite.regionY, mainSprite.regionWidth, mainSprite.regionHeight)
        shadowSprite.setSize(size, size)
        shadowSprite.setOriginCenter()
        shadowSprite.setPosition(mainSprite.x- 2f, mainSprite.y- 2f)
        shadowSprite.rotation = mainSprite.rotation
        shadowSprite.setColor(Color.BLACK)
        shadowSprite.setAlpha(0.7f)

        rectangle = Rectangle(mainSprite.x, mainSprite.y, size, size)
    }

    fun update() {
        shadowSprite.setRegion(mainSprite.regionX, mainSprite.regionY, mainSprite.regionWidth, mainSprite.regionHeight)
        shadowSprite.setSize(size, size)
        shadowSprite.setOriginCenter()
        shadowSprite.setPosition(mainSprite.x- 2f, mainSprite.y- 2f)
        shadowSprite.rotation = mainSprite.rotation
        shadowSprite.setColor(Color.BLACK)
        shadowSprite.setAlpha(0.7f)

        rectangle.setPosition(mainSprite.x, mainSprite.y)
    }

    fun getRectangle() = rectangle

    fun draw(batch: SpriteBatch?) {
        mainSprite.draw(batch)
    }

    fun drawShadow(batch: SpriteBatch?) {
        shadowSprite.draw(batch)
    }
}