package com.lowder.libgdxsimplesnake.utils

import com.badlogic.gdx.math.Vector2
import kotlin.math.atan2

/**
 * Created by AntonyCS on 07/04/2024
 */
fun Vector2.getRotationCust(targetVector2: Vector2, degCorrection: Float = 180.0f)=  ((atan2(
    targetVector2.x - this.x,
    -(targetVector2.y - this.y)
) * 180.0 / Math.PI) + degCorrection).toFloat()