package com.lowder.libgdxsimplesnake.food

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.lowder.libgdxsimplesnake.config.GRID_MAX_X
import com.lowder.libgdxsimplesnake.config.GRID_MAX_Y
import com.lowder.libgdxsimplesnake.config.GRID_MIN_X
import com.lowder.libgdxsimplesnake.config.GRID_MIN_Y
import com.lowder.libgdxsimplesnake.config.GRID_SIZE

/**
 * Created by AntonyCS on 07/04/2024
 */
class FoodPool {
    private val texture: Texture = Texture("food/food_1.png")
    private val foods: MutableList<Food> = mutableListOf()

    private var updateTime: Float = 0f
    var countFood: Int = 0
    private var maxFood: Int = 1


    fun getList() = foods

    fun update(delta: Float, snakePositions: List<Vector2>) {
        updateTime += delta
        if (updateTime > 1f && countFood < maxFood) {
            updateTime = 0f

            val foodType = Food.APPLE_FOOD_TYPE
            val pos = getSpawnPos(snakePositions)
            foods.forEach {
                if (!it.isAlive) {
                    it.reuse(pos, foodType)
                    countFood++
                    return
                }
            }
            foods.add(Food(texture, pos, foodType))
            countFood++
        }
    }

    private fun getSpawnPos(snakePositions: List<Vector2>): Vector2 {
        val randX = MathUtils.random(GRID_MIN_X, GRID_MAX_X) * GRID_SIZE
        val randY = MathUtils.random(GRID_MIN_Y, GRID_MAX_Y) * GRID_SIZE
        var pos = Vector2(randX, randY)

        var isInvalid = false
        snakePositions.forEach {
            val snakePosX = it.x
            val snakePosY = it.y
            if (randX == snakePosX && randY == snakePosY) {
                isInvalid = true
                return@forEach
            }
        }
        if (isInvalid) {
            pos = getSpawnPos(snakePositions)
        }
        return pos
    }

    fun draw(batch: SpriteBatch?) {
        foods.forEach { it.draw(batch) }
    }

    fun dispose() {
        texture.dispose()
    }
}