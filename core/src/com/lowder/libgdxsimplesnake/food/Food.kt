package com.lowder.libgdxsimplesnake.food

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.lowder.libgdxsimplesnake.config.GRID_SIZE
import com.lowder.libgdxsimplesnake.config.SpriteWithShadow
import com.lowder.libgdxsimplesnake.data.FoodData

/**
 * Created by AntonyCS on 07/04/2024
 */
class Food(
    texture: Texture,
    private var pos: Vector2,
    foodType: Int
) {
    private var spriteWithShadow: SpriteWithShadow
    var growPoint = 1
    var isAlive = true

    init {
        var textureRectangle = Rectangle(0f, 0f, 23f, 23f)
        FOOD_DATA_TYPES[foodType]?.let {
            growPoint = it.growPoint
            textureRectangle = it.textureRegion
        }
        spriteWithShadow = SpriteWithShadow(
            Sprite(texture, textureRectangle.x.toInt(), textureRectangle.y.toInt(), textureRectangle.width.toInt(), textureRectangle.height.toInt()).apply {
                setSize(GRID_SIZE, GRID_SIZE)
                setOriginCenter()
                setPosition(pos.x, pos.y)
            }
        )
    }

    fun reuse(pos: Vector2, foodType: Int) {
        this.isAlive = true
        this.pos = pos

        var textureRectangle = Rectangle(0f, 0f, 23f, 23f)
        FOOD_DATA_TYPES[foodType]?.let {
            growPoint = it.growPoint
            textureRectangle = it.textureRegion
        }
        spriteWithShadow.mainSprite.apply {
            setRegion(textureRectangle.x.toInt(), textureRectangle.y.toInt(), textureRectangle.width.toInt(), textureRectangle.height.toInt())
            setSize(GRID_SIZE, GRID_SIZE)
            setOriginCenter()
            setPosition(pos.x, pos.y)
        }
        spriteWithShadow.update()
    }

    fun getRectangle() = spriteWithShadow.getRectangle()

    fun draw(batch: SpriteBatch?) {
        if (isAlive) {
            spriteWithShadow.drawShadow(batch)
            spriteWithShadow.draw(batch)
        }
    }

    companion object {
        const val APPLE_FOOD_TYPE = 0

        val FOOD_DATA_TYPES = mapOf(
            APPLE_FOOD_TYPE to FoodData(5, Rectangle(0f, 0f, 23f, 23f))
        )
    }
}