package com.lowder.libgdxsimplesnake.data

import com.badlogic.gdx.math.Rectangle

/**
 * Created by AntonyCS on 07/04/2024
 */
data class FoodData(
    var growPoint: Int,
    var textureRegion: Rectangle
)