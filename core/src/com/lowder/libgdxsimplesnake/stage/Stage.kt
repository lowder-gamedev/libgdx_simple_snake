package com.lowder.libgdxsimplesnake.stage

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.lowder.libgdxsimplesnake.config.ARENA_HEIGHT
import com.lowder.libgdxsimplesnake.config.ARENA_WIDTH

/**
 * Created by AntonyCS on 07/04/2024
 */
class Stage {
    private var texture: Texture = Texture("stage/stage_1.png")

    fun draw(batch: SpriteBatch?) {
        batch?.draw(texture, 0f, 0f, ARENA_WIDTH.toFloat(), ARENA_HEIGHT.toFloat())
    }

    fun dispose() {
        texture.dispose()
    }
}