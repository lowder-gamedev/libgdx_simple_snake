package com.lowder.libgdxsimplesnake

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.ScreenUtils
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.lowder.libgdxsimplesnake.config.ARENA_HEIGHT
import com.lowder.libgdxsimplesnake.config.ARENA_WIDTH
import com.lowder.libgdxsimplesnake.config.CAMERA_OFFSET_Y
import com.lowder.libgdxsimplesnake.config.SNAKE_SPAWN_X
import com.lowder.libgdxsimplesnake.config.SNAKE_SPAWN_Y
import com.lowder.libgdxsimplesnake.food.FoodPool
import com.lowder.libgdxsimplesnake.snake.Snake
import com.lowder.libgdxsimplesnake.stage.Stage
import com.lowder.libgdxsimplesnake.ui.MainUI

/**
 * Created by AntonyCS on 31/03/2024
 */
class SimpleSnakeMain : ApplicationAdapter() {

    private var batch: SpriteBatch? = null
    private var camera: OrthographicCamera? = null
    private var viewport: ExtendViewport? = null

    private lateinit var snake: Snake
    private lateinit var foodPool: FoodPool
    private lateinit var stage: Stage
    private lateinit var mainUI: MainUI

    override fun create() {
        batch = SpriteBatch()
        camera = OrthographicCamera(ARENA_WIDTH.toFloat(), ARENA_HEIGHT.toFloat())
        camera?.position?.set(ARENA_WIDTH / 2f, (ARENA_HEIGHT - CAMERA_OFFSET_Y) / 2f, 0f)
        camera?.update()
        viewport = ExtendViewport(ARENA_WIDTH.toFloat(), ARENA_HEIGHT.toFloat(), camera)

        snake = Snake(Vector2(SNAKE_SPAWN_X, SNAKE_SPAWN_Y))
        foodPool = FoodPool()
        stage = Stage()
        mainUI = MainUI()
    }

    private fun update(delta: Float) {
        if (Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP)) {
            snake.setMove(Snake.MOVE_UP, Snake.MOVE_DOWN)
        } else if (Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            snake.setMove(Snake.MOVE_DOWN, Snake.MOVE_UP)
        } else if (Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            snake.setMove(Snake.MOVE_LEFT, Snake.MOVE_RIGHT)
        } else if (Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            snake.setMove(Snake.MOVE_RIGHT, Snake.MOVE_LEFT)
        }

        foodPool.update(delta, snake.getAllBodyPosition())
        snake.update(delta, foodPool.getList()) {
            foodPool.countFood--
        }
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewport?.update(width, height)
    }

    override fun render() {
        val delta = Gdx.graphics.deltaTime
        update(delta)

        ScreenUtils.clear(0f, 0f, 0f, 1f)
        batch?.projectionMatrix = camera?.combined
        batch?.begin()
        stage.draw(batch)

        mainUI.draw(batch)

        foodPool.draw(batch)
        snake.draw(batch)

        batch?.end()
    }

    override fun dispose() {
        batch?.dispose()
        snake.dispose()
        stage.dispose()
        foodPool.dispose()
    }
}