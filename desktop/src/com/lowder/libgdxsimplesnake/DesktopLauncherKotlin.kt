package com.lowder.libgdxsimplesnake

import com.badlogic.gdx.Graphics
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import com.lowder.libgdxsimplesnake.config.ARENA_HEIGHT
import com.lowder.libgdxsimplesnake.config.ARENA_WIDTH

/**
 * Created by AntonyCS on 31/03/2024
 */
fun main() {
    val config = Lwjgl3ApplicationConfiguration()
    config.setForegroundFPS(60)
    config.setTitle("Libgdx Simple Snake")
    config.setWindowedMode(ARENA_WIDTH, ARENA_HEIGHT)
    Lwjgl3Application(SimpleSnakeMain(), config)
}